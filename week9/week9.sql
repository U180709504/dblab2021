use company;

select * from Customers;

update Customers set Country = replace(Country, '\n', '');
update Customers set city = replace(country, '\n', '');

create view mexicanCustomers as
select customerid, customername, contactname
from Customers
where Country = "Mexico";

select * from mexicanCustomers;

select *
from mexicanCustomers join Orders on mexicanCustomers.CustomerID = Orders.CustomerID;

create view productbelowavg as
select ProductID, ProductName, Price
from Products
where Price < (select avg(Price) from Products) ;

delete from OrderDetails where ProductID=5;
truncate OrderDetails;

delete from Customers;