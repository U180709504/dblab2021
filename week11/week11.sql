call selectAllCustomers();

call getCustomersByCountry("Mexico");

set @country = "UK";

call getCustomersByCountry(@country);

select count(OrderID) from Orders join Shippers on Orders.ShipperID = Shippers.ShipperID
where ShipperName = "Speedy Express";

set @orderCount = 0;
call getNumberOfOrdersByShipper("Speedy Express", @orderCount);
select @orderCount;

set @beg = 100;
set @inc = 10;

call counter(@beg,@inc);
select @beg, @inc;

select * from movies;

load data infile 'C:\\ProgramData\\MySQL\\MySQL Server 8.0\\Uploads\\denormalized.csv'
into table denormalized
columns terminated by ';';

show variables like "secure_file_priv";

insert into movies (movie_id, title, ranking, rating, year, votes, duration, oscars, budget)
select distinctrow movie_id, title, ranking, rating, year, votes, duration, oscars, budget
from denormalized;

select * from movies;

insert into countries(country_id, country_name)
select distinctrow producer_country_id, producer_country_name
from denormalized
union
select distinctrow director_country_id, director_country_name
from denormalized
order by producer_country_id;

select * from countries;

